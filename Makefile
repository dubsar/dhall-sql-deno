logs:
	deployctl logs --project=dhall-sql

deploy:
	deployctl deploy --project=dhall-sql ./src/serve.ts

.PHONY: logs deploy

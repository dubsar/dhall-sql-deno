import { ConnInfo } from "https://deno.land/std@0.180.0/http/server.ts";

interface AccessEntry {
  ip: string;
  city: string;
  region: string;
  country: string;
  timezone: string;
}

const log = ({ ip, city, region, country, timezone }: AccessEntry) => {
  console.info("access: ", ip, city, region, country, timezone);
};

const url = (ip: string) => `https://ipinfo.io/${ip}/geo`;

const collect = async (ip: string): Promise<AccessEntry> => {
  const response = await fetch(url(ip));
  const { status } = response;
  if (status !== 200) {
    console.debug(`Error fetching ip: ${ip}`);
    throw `Error fetching ip: ${ip}`;
  } else {
    const entry: AccessEntry = await response.json();
    return entry;
  }
};

export class Access {
  private map: Map<string, AccessEntry> = new Map();

  register = async (connInfo: ConnInfo): Promise<void> => {
    try {
      const { hostname: ip } = connInfo.remoteAddr as Deno.NetAddr;
      const _ = this.map.get(ip) ?? await this.cache(ip);
    } catch (_) { /**/ }
  };

  private cache = async (ip: string): Promise<AccessEntry> => {
    const entry = await collect(ip);
    this.map.set(ip, entry);
    log(entry);
    return entry;
  };
}

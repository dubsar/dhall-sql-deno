/*
    https://dhall-sql.deno.dev
*/

import { ConnInfo, serve } from "https://deno.land/std@0.177.0/http/server.ts";
import { serveFile } from "https://deno.land/std@0.180.0/http/file_server.ts";
import { Cache } from "./cache.ts";
import { Access } from "./access.ts";

const cache = new Cache();
const access = new Access();

const SQL_VERSION = "0.0.1";
const SQL_URL = ` https://gitlab.com/dubsar/dhall-sql/-/raw/main`;
const PRELUDE_URL = `https://prelude.dhall-lang.org`;

const handler = async (
  request: Request,
  connInfo: ConnInfo,
): Promise<Response> => {
  access.register(connInfo);
  const { pathname } = new URL(request.url);
  if (pathname.startsWith(`/${SQL_VERSION}/Prelude/`)) {
    const path = pathname.replace(`${SQL_VERSION}/Prelude/`, "");
    const url = `${PRELUDE_URL}${path}`;
    return await cache.get(url);
  }
  if (pathname.endsWith(".dhall")) {
    const url = `${SQL_URL}${pathname}`;
    return await cache.get(url);
  }
  const pathName = pathname.startsWith("/docs")
    ? `./public/${pathname}`
    : "./public/index.html";
  return serveFile(request, pathName);
};

serve(handler);

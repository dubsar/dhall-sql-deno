import { typeByExtension } from "https://deno.land/std@0.180.0/media_types/mod.ts";
import { fileExtension } from "https://deno.land/x/file_extension@v2.1.0/mod.ts";

interface CacheEntry {
  url: string;
  body: string;
  headers: HeadersInit;
}

const mime = (url: string) =>
  typeByExtension(fileExtension(url)) ||
  "text/plain; charset=utf-8";

const headers = (url: string): HeadersInit => ({
  "content-type": mime(url),
  "x-xss-protection": "1; mode=block",
});

const reply = ({ body, headers }: CacheEntry): Response =>
  new Response(body, { status: 200, headers });

const collect = async (url: string): Promise<CacheEntry> => {
  const response = await fetch(url);
  const { status } = response;
  if (status !== 200) {
    throw "Invalid Request";
  } else {
    const body = await response.text();
    return { url, body, headers: headers(url) };
  }
};

export class Cache {
  private map: Map<string, CacheEntry> = new Map();

  get = async (url: string): Promise<Response> => {
    const entry = this.map.get(url) ?? await this.cache(url);
    return reply(entry);
  };

  private cache = async (url: string): Promise<CacheEntry> => {
    const entry = await collect(url);
    this.map.set(url, entry);
    console.info("cached:", url);
    return entry;
  };
}
